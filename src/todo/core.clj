(ns todo.core
  (:require [clojure.data.json :as json])
  (:require [clojure.string :as string])
  (:gen-class))

(defn add-todo [todos item]
  (when-not (some #(= (:title %) item) todos)
    (conj todos {:title item, :done false})))

(defn check-todo [todos index]
  (let [item (todos index)]
    (when item (assoc
                 todos index
                 (assoc item :done
                        (not (:done item)))))))

(defn remove-todo [todos index]
  (vec (concat
         (subvec todos 0 index)
         (subvec todos (inc index)))))

(defn remove-checked [todos]
  (letfn [(remove-one [items]
            (let [found (first
                          (filter #(= (:done (second %)) true)
                                  (map-indexed vector items)))]
              (if found
                (remove-one
                  (remove-todo items (first found)))
                items)))]
    (remove-one todos)))

(defn print-todos [todos]
  (do
    (println (str "To-Do count: " (count todos)))
    (doseq [i (range (count todos))]
      (let [todo (todos i)]
        (println (str i ". [" (if (:done todo) "x" " ") "] " (:title todo)))))))

(defn load-todos [file]
  (json/read-str (slurp file)
                 :key-fn keyword))

(defn dump-todos [todos file]
  (spit file (json/write-str todos)))

(defn process-input [input todos]
  (let [parts (string/split input #" ")
        command (first parts)]
    (try
      (case command
        "add" (add-todo todos (subs input (inc (count command))))
        "check" (check-todo todos (Integer. (parts 1)))
        "remove" (remove-todo todos (Integer. (parts 1)))
        "remove-done" (remove-checked todos)
        "load" (load-todos (parts 1))
        "dump" (dump-todos todos (parts 1))
        "print" (print-todos todos)
        (println (str command "?")))
      (catch IndexOutOfBoundsException ex (println "index?"))
      (catch java.io.FileNotFoundException ex (println "file?"))
      (catch Exception ex (println "syntax?")))))

(defn ask []
  (do
    (print ">> ")
    (flush)
    (read-line)))

(defn -main
  [& args]

  (def todo-list (atom []))

  (loop [input (ask)]
    (when-not (= "quit" input)
      (let [result (process-input input @todo-list)]
        (when result (reset! todo-list result)))
      (recur (ask))))

  (println "Bye!"))
